<div class="pagecontainer">
{$tab_start}

{$countries_start}
<form method="post" action="{$formurl}" enctype="multipart/form-data">
<input type="hidden" name="sync" value="countries1" />
<div class="pageoverflow">
    <p class="pageinput">{$sync_countries}</p>
</div>
</form>
{* <div style="clear:both" class="pageoptions"><p class="pageoptions">Sync</p></div> *}
<table cellspacing="0"  class="pagetable">
    <thead>
        <tr>
            <th>{lang('name')}</th>
            <th>{lang('alias')}</th>
            <th class="pageicon" style="width:50px;">{lang('default')}</th>
            <th class="pageicon" style="width:50px;">{lang('active')}</th>
            <th class="pageicon" style="width:50px;"></th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$countries item=country}
     {cycle values="row1,row2" assign='rowclass'}
     <tr class="{$rowclass}" onmouseover="this.className = '{$rowclass}hover';" onmouseout="this.className = '{$rowclass}';">
        <td>{$country->name}</td>
        <td>{$country->currencyCode}</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    {/foreach}
    </tbody>
</table>
{$tab_end}

{$currencies_start}
<form method="post" action="{$formurl}" enctype="multipart/form-data">
<input type="hidden" name="sync" value="currencies" />
<div class="pageoverflow">
    <p class="pageinput">{$sync_currencies}</p>
</div>
</form>
<table cellspacing="0"  class="pagetable">
    <thead>
        <tr>
            <th>{lang('code')}</th>
            <th>{lang('name')}</th>
            <th class="pageicon" style="width:50px;">{lang('default')}</th>
            <th class="pageicon" style="width:50px;">{lang('active')}</th>
            <th class="pageicon" style="width:50px;"></th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$currencies item=currency}
     {cycle values="row1,row2" assign='rowclass'}
     <tr class="{$rowclass}" onmouseover="this.className = '{$rowclass}hover';" onmouseout="this.className = '{$rowclass}';">
        <td>{$currency->code}</td>
        <td>{$currency->name}</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    {/foreach}
    </tbody>
</table>

{$tab_end}
{$tabs_end}
</div>

