<?php

function smarty_function_cms_help($params,&$template)
{
  $smarty = $template->smarty;

  $out = cms_admin_utils::get_help_tag($params);

  if( isset($params['assign']) ) {
    $smarty->assign($params['assign'],$out);
  }
  else {
    return $out;
  }
}
