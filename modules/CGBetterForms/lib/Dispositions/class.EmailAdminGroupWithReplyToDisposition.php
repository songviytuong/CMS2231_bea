<?php
namespace CGBetterForms\Dispositions;
use \CGBetterForms\utils;

class EmailAdminGroupWithReplyToDisposition extends Disposition
{
    private $_group;
    private $_fieldname;
    private $_subjecttemplate;
    private $_bodytemplate;

    public function __get($key)
    {
        switch( $key ) {
        case 'group':
            return $this->_group;
        case 'fieldname':
            return $this->_fieldname;
        case 'subjecttemplate':
            return trim($this->_subjecttemplate);
        case 'bodytemplate':
            return trim($this->_bodytemplate);
        }
    }

    public function set_group( $gid )
    {
        $this->_group = (int) $gid;
    }

    public function get_group()
    {
        return $this->_group;
    }

    public function set_fieldname( $email )
    {
        $this->_fieldname = trim($email);
    }

    public function get_fieldname()
    {
        return $this->_fieldname;
    }

    public function set_subjecttemplate( $data )
    {
        $this->_subjecttemplate = trim($data);
    }

    public function set_bodytemplate( $data )
    {
        $this->_bodytemplate = trim($data);
    }

    public function dispose( \CGBetterForms\Form $form, \CGBetterForms\FormResponse& $response)
    {
        $list = null;
        if( $this->_group > 0 ) $list = \cge_userops::expand_group_emails( $this->_group );
        if( !$list ) throw new \RuntimeException('No valid email addresses to send to for EmailAdminGroupDisposition');

        if( !$this->fieldname ) return;
        $replyto = $response->get_field_value($this->fieldname);
        if( !$replyto ) $replyto = $response->replyto_address;
        if( !$replyto || !is_email($replyto) ) return;

        $mailer = \CGBetterForms\utils::get_mailer( $form );
        $mailer->ClearReplyTos();
        $mailer->AddReplyTo($replyto);
        $added = false;
        foreach( $list as $addr ) {
            $addr = trim($addr);
            if( !$addr ) continue;
            if( !is_email( $addr) ) continue;
            $mailer->AddAddress( $addr );
            $added = true;
        }
        if( !$added ) throw new \RuntimeException('No valid email addresses to send to for EmailAdminGroupDisposition');
        $subject = trim(utils::process_template( $this->subjecttemplate, $form, $response));
        $body = trim(utils::process_template( $this->bodytemplate, $form, $response));
        if( !$subject || !$body ) return;
        $mailer->SetSubject( $subject );
        $mailer->SetBody( $body );
        $mailer->Send();
    }
} // end of class
