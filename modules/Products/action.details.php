<?php

if (!isset($gCms)) {
    exit;
}

$thetemplate = $this->find_layout_template($params, 'detailtemplate', 'Products::Detail View');
$cache_id = '|pd' . md5(serialize($params));
$tpl = $this->CreateSmartyTemplate($thetemplate, null, $cache_id);
if (!$tpl->isCached()) {
    $query = new products_query();
    if (isset($params['productid'])) {
        $query['productid'] = $params['productid'];
    } elseif (isset($params['alias'])) {
        $query['alias'] = $params['alias'];
    }
    $rs = $query->execute();
    $rs->curpage = $returnid;
    if ($rs->totalrows == 1) {
        $onerow = $rs->get_product_for_display();
        if ($onerow->hierarchy_id > 0) {
            $tpl->assign('active_hierarchy', $onerow->hierarchy_id);
        }
        $tpl->assign('weight_units', product_ops::get_weight_units());
        $tpl->assign('currency_symbol', product_ops::get_currency_symbol());
        $tpl->assign('entry', $onerow);
    } else {
        // product not found for some reason.
        $action = $this->GetPreference('prodnotfound', 'domsg');
        switch ($action) {
            case 'do404':
                throw new CmsError404Exception('product not found');
                break;

            case 'do301':
                $page = $this->GetPreference('prodnotfoundpage', -1);
                if ($page != '' && $page != -1) {
                    cge_redirect::redirect301($page);
                }
            // fall through to domsg
            // no break
            case 'domsg':
            default:
                $msg = $this->GetPreference('prodnotfoundmsg', $this->Lang('error_product_notfound'));
                echo $this->ProcessTemplateFromData($msg);

                return;
        }
    }
}

$tpl->display();
