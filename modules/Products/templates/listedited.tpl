{if $itemcount gt 0}
  {$startform}
  <table cellspacing="0" class="pagetable">
    <thead>
      <tr>
        <th>{$mod->Lang('id')}</th>
        <th data-ignore="highlight" data-hide="phone,tablet" data-sort-ignore="true" data-class="hide-heading">{$mod->Lang('productname')}</th>
        <th data-ignore="highlight" data-hide="phone,tablet" data-sort-ignore="true" data-class="hide-heading">{$mod->Lang('productname')} (VN)</th>
        <th>{$mod->Lang('sku')}</th>
        <th>{$mod->Lang('modifieddate')}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      {foreach from=$items item=entry}
	<tr id="item_{$entry->id}" class="{cycle values='row1,row2'}" style="cursor: move;">
        <td>{$entry->id}</td>
        <td data-ignore="highlight" data-hide="phone,tablet" data-sort-ignore="true" data-class="hide-heading"><a href="{$entry->edit_url}">{$entry->product_name}</a></td>
        <td data-ignore="highlight" data-hide="phone,tablet" data-sort-ignore="true" data-class="hide-heading">{$entry->product_name_vi}</td>
	<td>{$entry->sku}</td>
	<td>{$entry->modified_date}</td>
        <td><a href="{$entry->edit_url}">{admin_icon icon='edit.gif' alt=""}</a></td>
        </tr>
      {/foreach}
    </tbody>
  </table>
{$endform}
{/if}
