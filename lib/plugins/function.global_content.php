<?php

function smarty_function_global_content($params, &$smarty)
{
	$smarty->assign('gcb_params',$params);
	$txt = $smarty->fetch('cms_template:'.$params['name']);
	if( isset($params['assign']) )
	{
		$smarty->assign(trim($params['assign']),$txt);
		return;
	}
	return $txt;
}
