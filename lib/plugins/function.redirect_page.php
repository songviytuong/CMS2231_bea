<?php

function smarty_function_redirect_page($params, &$smarty)
{
	if( isset($params['page']) ) redirect_to_alias(trim($params['page']));
}
