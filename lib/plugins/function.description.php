<?php

function smarty_function_description($params, &$smarty)
{
	$gCms = CmsApp::get_instance();
	$content_obj = $gCms->get_content_object();

	if (!is_object($content_obj) || $content_obj->Id() == -1) {
		// We've a custom error message...  set a message
		$out = "404 Error";
	} else {
		$result = $content_obj->TitleAttribute();
		$out = preg_replace("/\{\/?php\}/", "", $result);
	}

	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $out);
		return;
	}
	return $out;
}

function smarty_cms_about_function_description()
{
	?>
	<p>Author: Elijah Lofgren &lt;elijahlofgren@elijahlofgren.com&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>