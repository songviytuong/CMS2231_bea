<?php

/**
 * Miscellaneous support functions
 *
 * @package CMS
 * @license GPL
 */


if (!function_exists('gzopen')) {
    /**
     * Wrapper for gzopen in case it does not exist.
     * Some installs of PHP (after PHP 5.3 use a different zlib library, and therefore gzopen is not defined.
     * This method works past that.
     *
     * @since 2.0
     * @ignore
     */
    function gzopen($filename, $mode, $use_include_path = 0)
    {
        return gzopen64($filename, $mode, $use_include_path);
    }
}
