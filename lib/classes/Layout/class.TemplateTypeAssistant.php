<?php

namespace CMSMS\Layout;

abstract class TemplateTypeAssistant
{
    /**
     * Get the type object for the current assistant.
     *
     * @return CmsLayoutTemplateType
     */
    abstract public function &get_type();

    /**
     * Get a usage string for the current assistant.
     *
     * @param string $name The template name.
     * @return string
     */
    abstract public function get_usage_string($name);
}
